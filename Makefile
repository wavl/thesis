COUNT:=$(shell texcount thesis.tex -char -inc | grep "Letters in text:" | tail -n1 | cut -f2- -d:)

count:
	@echo Does not count spaces! The number is therefore lower than actual number of standard pages.
	@echo Letters: $(COUNT)
	@echo Pages: $(shell echo "scale=2; $(COUNT)/1800.0" | bc -l )/30

upload:
	rsync thesis.pdf poincare:public_html/wavl/thesis.pdf